# Intro_to_simulation_algorithm

In the file provided there are some  basic operation :

- The use of the Linear Congruential Generator to generate uniform pseudo random variables


- The use of Kolmogorov-Smirnov test


- The use of Die Hard test (using a default generator)


- The use of uniform law with runif() function


- The use of discreet uniform law with floor() and runif()


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021
